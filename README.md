# README #

### Result ###

Customer	  | Event         | Participant   | Stake         | ToWin
------------- | ------------- | ------------- | ------------- | -------------
**1**         |	11            |	4             |	50            |	500
**1**         |	12            |	4             |	**500**       |	**5000**
**1**         |	13            |	3             |	50            |	200
**1**         |	14            |	2             |	**1000**      |	**8000**
**3**         |	11            |	6             |	50            |	400
**3**         |	12            |	1             |	50            |	400
**3**         |	13            |	9             |	40            |	400
**3**         |	14            |	8             |	300           |	900
**4**         |	11            |	7             |	300           |	**1200**
**4**         |	12            |	1             |	250           |	**1000**
**4**         |	13            |	6             |	200           |	**1000**
**4**         |	14            |	5             |	200           |	800
**6**         |	12            |	3             |	50            |	400
**6**         |	12            |	2             |	50            |	200
**6**         |	13            |	4             |	**500**       |	**4000**
**6**         |	14            |	6             |	50            |	400
				
### How do I get set up? ###

* Make sure you have Visual Studio 2015 installed in your dev environment
* Fork this repository and pull it down to your local environment (Git or SourceTree)
* Open /src/BetAnalysisTool/BetAnalysisTool.sln
* Build the whole solution, make sure all nuget packages have been restore.
* If build succeed, run BAT.BetAnalysisTool

### Who do I talk to? ###

* tigerhaolu727@hotmail.com