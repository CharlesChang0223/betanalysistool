﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

namespace BAT.BetAnalysisTool.Helpers
{
    public class CSVParser
    {
        public IList<T> Parse<T>(FileInfo fileInfo, CsvClassMap<T> classMap) where T : class
        {
            using (TextReader reader = File.OpenText(fileInfo.FullName))
            {
                var csv = new CsvReader(reader);
                csv.Configuration.RegisterClassMap(classMap);
                return csv.GetRecords<T>().ToList();
            }
        }
    }
}
