﻿using BAT.ApplicationService.Dtos;
using CsvHelper.Configuration;

namespace BAT.BetAnalysisTool.Helpers
{
    public sealed class UnsettledBetDtoMap : CsvClassMap<UnsettledBetDto>
    {
        public UnsettledBetDtoMap()
        {
            Map(m => m.CustomerId).Name("Customer");
            Map(m => m.EventId).Name("Event");
            Map(m => m.ParticipantId).Name("Participant");
            Map(m => m.Stake).Name("Stake");
            Map(m => m.ToWin).Name("To Win");
        }
    }
}