﻿using System.Collections.Generic;
using System.Linq;
using BAT.Domain.Entities;
using BAT.Domain.IdentifyRules.RiskBetIdentifyRules;
using BAT.Domain.ValueObjects;

namespace BAT.Domain.Services
{
    public class RiskBetIdentifyService
    {
        private readonly RiskBetIdentifyRule _riskBetIdentifyRule;

        public RiskBetIdentifyService()
        {
            _riskBetIdentifyRule = new UnusualWinRateCustomerRiskBetIdentifyRule();

            var unusualRiskBetIdentifyRule = new UnusualRiskBetIdentifyRule();
            _riskBetIdentifyRule.SetNextRiskBetIdentifyRule(unusualRiskBetIdentifyRule);

            var hightToWinRiskBetIdentifyRule = new HightToWinRiskBetIdentifyRule();
            unusualRiskBetIdentifyRule.SetNextRiskBetIdentifyRule(hightToWinRiskBetIdentifyRule);
        }

        public IList<RiskBet> DetectRiskBets(Customer customer)
        {
            return customer.UnsettledBets.Select(RiskBet.Create).Select(riskBet => _riskBetIdentifyRule.DetectRiskBets(customer, riskBet)).Where(riskBet => riskBet.HasRisk).ToList();
        }
    }
}
