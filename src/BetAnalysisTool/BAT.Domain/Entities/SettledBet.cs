﻿using BAT.Domain.ValueObjects;

namespace BAT.Domain.Entities
{
    public class SettledBet: Bet
    {
        private SettledBet(CustomerId customerId, EventId eventId, ParticipantId participantId, decimal stake, decimal win) : base(customerId, eventId, participantId, stake)
        {
            Win = win;
        }

        public decimal Win { get; private set; }

        public static SettledBet Create(CustomerId customerId, EventId eventId, ParticipantId participantId, decimal stake, decimal win)
        {
            return new SettledBet(customerId, eventId, participantId,stake,win);
        }
    }
}
