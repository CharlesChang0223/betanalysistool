﻿using System;
using BAT.Domain.ValueObjects;

namespace BAT.Domain.Entities
{
    public abstract class Bet : IEquatable<Bet>
    {
        public bool Equals(Bet other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return CustomerId.Equals(other.CustomerId) && EventId.Equals(other.EventId) && ParticipantId.Equals(other.ParticipantId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Bet) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = CustomerId.GetHashCode();
                hashCode = (hashCode*397) ^ EventId.GetHashCode();
                hashCode = (hashCode*397) ^ ParticipantId.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Bet left, Bet right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Bet left, Bet right)
        {
            return !Equals(left, right);
        }

        protected Bet(CustomerId customerId, EventId eventId, ParticipantId participantId, decimal stake)
        {
            CustomerId = customerId;
            EventId = eventId;
            ParticipantId = participantId;
            Stake = stake;
        }

        public CustomerId CustomerId { get; }
        public EventId EventId { get; }
        public ParticipantId ParticipantId { get; }
        public decimal Stake { get; private set; }
    }
}
