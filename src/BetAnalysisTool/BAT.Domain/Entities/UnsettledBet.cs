﻿using BAT.Domain.ValueObjects;

namespace BAT.Domain.Entities
{
    public class UnsettledBet: Bet
    {
        public UnsettledBet(CustomerId customerId, EventId eventId, ParticipantId participantId, decimal stake, decimal toWin) : base(customerId, eventId, participantId, stake)
        {
            ToWin = toWin;
        }

        public decimal ToWin { get; private set; }

        public static UnsettledBet Create(CustomerId customerId, EventId eventId, ParticipantId participantId, decimal stake, decimal toWin)
        {
            return new UnsettledBet(customerId,eventId,participantId,stake,toWin);
        }
    }
}
