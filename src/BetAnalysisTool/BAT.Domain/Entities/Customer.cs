﻿using System;
using System.Collections.Generic;
using System.Linq;
using BAT.Domain.ValueObjects;

namespace BAT.Domain.Entities
{
    public class Customer
    {
        private readonly IList<UnsettledBet> _unsettledBets;
        private readonly IList<SettledBet> _settledBets; 
        private Customer(CustomerId id)
        {
            Id = id;
            _unsettledBets = new List<UnsettledBet>();
            _settledBets = new List<SettledBet>();
        }

        public CustomerId Id { get; private set; }

        public IReadOnlyList<UnsettledBet> UnsettledBets
        {
            get { return (IReadOnlyList<UnsettledBet>)_unsettledBets; }
        }

        public IReadOnlyList<SettledBet> SettledBets
        {
            get { return (IReadOnlyList<SettledBet>)_settledBets; }
        }

        public void AddSettledBet(SettledBet settledBet)
        {
            if(settledBet.CustomerId != Id)
                throw new ArgumentException($"Invalid SettledBet[CustomerId {settledBet.CustomerId}] for Customer {Id}.");

            if (!_settledBets.Contains(settledBet))
            {
                _settledBets.Add(settledBet);
            }
        }

        public void AddUnsettledBet(UnsettledBet unsettledBet)
        {
            if (unsettledBet.CustomerId != Id)
                throw new ArgumentException($"Invalid UnsettledBet[CustomerId {unsettledBet.CustomerId}] for Customer {Id}.");

            if (!_unsettledBets.Contains(unsettledBet))
            {
                _unsettledBets.Add(unsettledBet);
            }
        }

        private decimal TotalSettledWin { get { return _settledBets.Sum(x => x.Win); } }
        private decimal TotalSettledStake { get { return _settledBets.Sum(x => x.Stake); } }

        public decimal WinningRate
        {
            get { return (TotalSettledWin - TotalSettledStake)/TotalSettledStake; } 
        }

        public decimal AverageBet
        {
            get { return TotalSettledStake / _settledBets.Count; }
        }

        public static Customer Create(CustomerId id)
        {
            return new Customer(id);
        }
    }
}
