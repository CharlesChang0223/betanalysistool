﻿using System;

namespace BAT.Domain.ValueObjects
{
    public struct CustomerId : IEquatable<CustomerId>
    {
        public bool Equals(CustomerId other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is CustomerId && Equals((CustomerId) obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public static bool operator ==(CustomerId left, CustomerId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(CustomerId left, CustomerId right)
        {
            return !left.Equals(right);
        }

        private CustomerId(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }

        public static CustomerId CreateFrom(string idfield)
        {
            int id;
            if (!int.TryParse(idfield, out id))
                throw new ArgumentException("Invalid value for CustomerId");
            return Create(id);
        }

        public override string ToString()
        {
            return Id.ToString();
        }

        public static CustomerId Create(int id)
        {
            return new CustomerId(id);
        }
    }
}
