﻿using System;

namespace BAT.Domain.ValueObjects
{
    [Flags]
    public enum RiskLevel
    {
        None = 0,
        RiskCustomer = 1,
        UnusualStake = 2,
        HighlyUnusualStake = 4,
        HighToWin = 8
    }
}
