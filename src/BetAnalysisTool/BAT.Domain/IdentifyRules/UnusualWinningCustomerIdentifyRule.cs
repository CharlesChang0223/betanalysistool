﻿using BAT.Domain.Entities;

namespace BAT.Domain.IdentifyRules
{
    public class UnusualWinningCustomerIdentifyRule
    {
        private readonly double UnusualWinningPercentage = 0.6;

        public bool IsUnusualWinningCustomer(Customer customer)
        {
            return (double)customer.WinningRate >= UnusualWinningPercentage;
        }
    }
}
