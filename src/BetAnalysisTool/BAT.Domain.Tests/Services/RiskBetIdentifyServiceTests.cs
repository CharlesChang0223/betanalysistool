﻿using System.Linq;
using BAT.Domain.Entities;
using BAT.Domain.Services;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.Services
{
    [TestFixture]
    public class RiskBetIdentifyServiceTests
    {
        [Test]
        public void DetectRiskBets_CustomerWithNormalBet_ReturnEmptyList()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 300);
            customer.AddSettledBet(settledBet);
            var unsettledEventId = EventId.Create(201);
            var unsettledParticipantId = ParticipantId.Create(301);
            var unsettledBet = UnsettledBet.Create(customerId, unsettledEventId, unsettledParticipantId, 250, 800);
            customer.AddUnsettledBet(unsettledBet);
            var service = new RiskBetIdentifyService();
            var results = service.DetectRiskBets(customer);
            Assert.IsEmpty(results);
        }

        [Test]
        public void DetectRiskBets_UnusualWinRateCustomer_ReturnAllCustomerUnSettledBetsAsRiskyBet()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 400);
            customer.AddSettledBet(settledBet);
            var unsettledEventId = EventId.Create(201);
            var unsettledParticipantId = ParticipantId.Create(301);
            var unsettledBet = UnsettledBet.Create(customerId, unsettledEventId, unsettledParticipantId, 250, 800);
            customer.AddUnsettledBet(unsettledBet);
            var service = new RiskBetIdentifyService();
            var results = service.DetectRiskBets(customer);
            Assert.True(results.Count == 1);
            Assert.AreEqual(unsettledBet, results.First().UnsettledBet);
            Assert.AreEqual(RiskLevel.RiskCustomer, results.First().RiskLevel);
        }

        [Test]
        public void DetectRiskBets_NormalWinRateCustomerWith20TimesAverageBet_ReturnCustomerUnSettledBetsAsUnusualRiskBet()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 300);
            customer.AddSettledBet(settledBet);
            var unsettledEventId = EventId.Create(201);
            var unsettledParticipantId = ParticipantId.Create(301);
            var unsettledBet = UnsettledBet.Create(customerId, unsettledEventId, unsettledParticipantId, 5000, 800);
            customer.AddUnsettledBet(unsettledBet);
            var service = new RiskBetIdentifyService();
            var results = service.DetectRiskBets(customer);
            Assert.True(results.Count == 1);
            Assert.AreEqual(unsettledBet, results.First().UnsettledBet);
            Assert.AreEqual(RiskLevel.UnusualStake, results.First().RiskLevel);
        }

        [Test]
        public void DetectRiskBets_NormalWinRateCustomerWith30TimesAverageBet_ReturnCustomerUnSettledBetsAsHighUnusualRiskBet()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 300);
            customer.AddSettledBet(settledBet);
            var unsettledEventId = EventId.Create(201);
            var unsettledParticipantId = ParticipantId.Create(301);
            var unsettledBet = UnsettledBet.Create(customerId, unsettledEventId, unsettledParticipantId, 7500, 800);
            customer.AddUnsettledBet(unsettledBet);
            var service = new RiskBetIdentifyService();
            var results = service.DetectRiskBets(customer);
            Assert.True(results.Count == 1);
            Assert.AreEqual(unsettledBet, results.First().UnsettledBet);
            Assert.AreEqual(RiskLevel.HighlyUnusualStake, results.First().RiskLevel);
        }

        [Test]
        public void DetectRiskBets_NormalWinRateCustomerWith1000ToWin_ReturnCustomerUnSettledBetsAsRiskyBet()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 300);
            customer.AddSettledBet(settledBet);
            var unsettledEventId = EventId.Create(201);
            var unsettledParticipantId = ParticipantId.Create(301);
            var unsettledBet = UnsettledBet.Create(customerId, unsettledEventId, unsettledParticipantId, 500, 1000);
            customer.AddUnsettledBet(unsettledBet);
            var service = new RiskBetIdentifyService();
            var results = service.DetectRiskBets(customer);
            Assert.True(results.Count == 1);
            Assert.AreEqual(unsettledBet, results.First().UnsettledBet);
            Assert.AreEqual(RiskLevel.HighToWin, results.First().RiskLevel);
        }

        [Test]
        public void DetectRiskBets_MultipleRiskBet_ReturnUnSettledBetsWithMutipleRisks()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 400);
            customer.AddSettledBet(settledBet);
            var unsettledEventId = EventId.Create(201);
            var unsettledParticipantId = ParticipantId.Create(301);
            var unsettledBet = UnsettledBet.Create(customerId, unsettledEventId, unsettledParticipantId, 7500, 10000);
            customer.AddUnsettledBet(unsettledBet);
            var service = new RiskBetIdentifyService();
            var results = service.DetectRiskBets(customer);
            Assert.True(results.Count == 1);
            Assert.AreEqual(unsettledBet, results.First().UnsettledBet);
            Assert.AreEqual(RiskLevel.RiskCustomer, results.First().RiskLevel & RiskLevel.RiskCustomer);
            Assert.AreEqual(RiskLevel.HighlyUnusualStake, results.First().RiskLevel & RiskLevel.HighlyUnusualStake);
            Assert.AreEqual(RiskLevel.HighToWin, results.First().RiskLevel & RiskLevel.HighToWin);
        }
    }
}
