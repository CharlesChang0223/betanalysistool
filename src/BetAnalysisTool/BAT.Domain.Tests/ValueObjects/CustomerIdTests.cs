﻿using System;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.ValueObjects
{
    [TestFixture]
    public class CustomerIdTests
    {
        [Test]
        public void Create_InvalidStringValue_ThrowArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => CustomerId.CreateFrom("AAA"));
            Assert.AreEqual("Invalid value for CustomerId", exception.Message);
        }

        [Test]
        public void Create_ValidStringValue_ReturnCustomerId()
        {
            var customerid = CustomerId.CreateFrom("111");
            Assert.IsTrue(customerid.Id == 111);
        }
    }
}
